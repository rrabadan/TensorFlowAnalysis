import sys, os, math
sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from TensorFlowAnalysis import *

if __name__ == "__main__" : 
    
    # Masses of the initial and final state particles
    mb   = 5.279
    mdst = 2.010
    mds  = 1.940
    mpi  = 0.139
  
    # Radial constants for Blatt-Weisskopf form factors
    db = Const(5.)
    dr = Const(1.5)
    j = 2
  
    # Set random seed
    SetSeed(1)
    
    # Fit phase space is the direct product of Dalitz and 2D angular phase space (in cos(theta) vs. phi)
    phsp = RectangularPhaseSpace( [(mdst+mpi, mb-mds)] )
  
    m1, gam1 = Const(2.4241), Const(0.0336)
    m2, gam2 = Const(2.311), Const(0.309)
    fsc = Const(0.0)
    
    def model1(x) : 
        m = phsp.Coordinate(x, 0)
        return Density(SingleChannelKMatrix(m*m, [m1, m2], [gam1, gam2], mdst, mpi, mds, mb, dr, db, 1, 1, fsc, barrierFactor=False))
    
    def model2(x) : 
        m = phsp.Coordinate(x, 0)
        ampl = BreitWignerLineShape(m*m, m1, gam1, mdst, mpi, mds, mb, dr, db, 1, 1) + BreitWignerLineShape(m*m, m2, gam2, mdst, mpi, mds, mb, dr, db, 1, 1) 
        return Density(ampl)

    # Placeholders for data and normalisation samples
    norm_ph = phsp.norm_placeholder
    
    # TF graphs for decay density as functions of data and normalisation placeholders
    norm_model_1 = model1(norm_ph)/Integral(model1(norm_ph))
    norm_model_2 = model2(norm_ph)/Integral(model2(norm_ph))

    #data = np.random.uniform(mdst+mpi, mb-mds, int(1e6))
    # Initialise TF
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
    
    norm_sample = sess.run( phsp.RectangularGridSample( (10000, ) ) )

    pdf1 = sess.run(norm_model_1, feed_dict={norm_ph: norm_sample})
    pdf2 = sess.run(norm_model_2, feed_dict={norm_ph: norm_sample})

    pdf1 = pdf1 #/np.max(pdf1)
    pdf2 = pdf2 #/np.max(pdf2)

    # Uniform mormalisation sample
    import matplotlib.pyplot as plt
    fig, ax = plt.subplots(nrows = 1, ncols = 2, figsize = (12, 6) )
    
    ax[0].hist(norm_sample, 50, weights=pdf1, density=True, facecolor='g', alpha=0.75)
    ax[1].hist(norm_sample, 50, weights=pdf2, density=True, facecolor='r', alpha=0.75)
    ax[0].grid(True)
    ax[1].grid(True)
    plt.show()
