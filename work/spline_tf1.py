from sympy import interpolating_spline
from sympy.abc import x
from sympy.utilities.lambdify import lambdify

import tensorflow as tf

n = [0,1,2,3,4,5]
c =  [3,6,5,7,9,1]

spl = interpolating_spline(3, x, n, c)
print(spl)

a = tf.constant([1., 2.])

f = lambdify(x, spl, 'tensorflow') 

sess = tf.Session()

print(sess.run(a))
print(sess.run(f(a)))
