import sys, os, math
sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""

from TensorFlowAnalysis import *

if __name__ == "__main__" : 
    
    # Masses of the initial and final state particles
    mb   = 5.279
    mdst = 2.010
    mds  = 1.940
    mpi  = 0.139
  
    # Radial constants for Blatt-Weisskopf form factors
    db = Const(5.)
    dr = Const(1.5)
  
    # Set random seed
    SetSeed(1)
    
    # Fit phase space is the direct product of Dalitz and 2D angular phase space (in cos(theta) vs. phi)
    dalitzPhsp = DalitzPhaseSpace(mdst, mpi, mds, mb)
    angularPhsp = RectangularPhaseSpace( ((-1., 1.), (-math.pi, math.pi)) )
    phsp = CombinedPhaseSpace(dalitzPhsp, angularPhsp)
  
    m1, gam1 = Const(2.4241), Const(0.0336)
    m2, gam2 = Const(2.311), Const(0.309)
    fsc = FitParameter("fsc", 0.0001, -5., 5., 0.01)
    
    ls_coup = { 
        (0,2) : Complex(FitParameter("D_01_re", 0.1, -5., 5., 0.01), FitParameter("D_01_im", 0.1, -5., 5., 0.01)),
        (4,2) : Complex(Const(20.), Const(0.))
    }

    def model(x) : 
        """
          Description of the decay density as a function of the vector of input 
          parameters x.
        """
        dlz = phsp.Data1(x)
        ang = phsp.Data2(x)
        m2dstpi = dalitzPhsp.M2ab(dlz)
        cos_th_dst = dalitzPhsp.CosHelicityAB(dlz)
        cos_th_d = angularPhsp.Coordinate(ang, 0)
        phi_d = angularPhsp.Coordinate(ang, 1)
        
        ls_ampl = { ls: coup*SingleChannelKMatrix(m2dstpi, [m1, m2], [gam1, gam2], mdst, mpi, mds, mb, dr, db, 1, 1, fsc, barrierFactor=False) for (ls,coup) in ls_coup.items() }
        ampl = Complex(Const(0.), Const(0.))

        j = 2
        for lm in [-2, 0, 2]:
            ampl += HelicityCouplingsFromLS(j, 2, 0,  lm, 0, ls_ampl)*\
                    HelicityAmplitude3Body(Acos(cos_th_dst), 0., Acos(cos_th_d), phi_d, j, 2, 0, lm, 0, 0, 0)

        # Density is the abs value squared of the amplitude
        return Density(ampl)
  
    # Initialise TF
    init = tf.global_variables_initializer()
    sess = tf.Session()
    sess.run(init)
  
    # Placeholders for data and normalisation samples
    data_ph = phsp.data_placeholder
    norm_ph = phsp.norm_placeholder
  
    # TF graphs for decay density as functions of data and normalisation placeholders
    data_model = model(data_ph)
    norm_model = model(norm_ph)
  
    # Uniform mormalisation sample
    norm_sample = sess.run( phsp.UniformSample(1000000) )
  
    # Calculate the maximum of PDF for accept-reject method
    majorant = EstimateMaximum(sess, norm_model, norm_ph, norm_sample)*1.5
    print("Maximum = ", majorant)
  
    # Run toy MC with the model defined above
    data_sample = RunToyMC( sess, norm_model, norm_ph, phsp, 100000, majorant, chunk = 1000000)
  
    # Store the result to ROOT file
    FillRootFile("toy.root", data_sample, [ "m2dstpi", "m2dspi", "costhd", "phid"] )
    exit()
  
    # Negative log likelihood for the fit
    nll = UnbinnedNLL( data_model, Integral( norm_model ) )
  
    # Perform Minuit minimisation
    result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = True )
  
    # Write results to text file
    WriteFitResults(result, "result.txt")
