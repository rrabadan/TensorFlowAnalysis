# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

# Example script for multidimensional probability density estimation using neural networks (multilayer perceptron)

import tensorflow as tf
from root_numpy import rec2array

import sys, os
sys.path.append("../")
#os.environ["CUDA_VISIBLE_DEVICES"] = ""
from TensorFlowAnalysis import *

def main() : 

  SetSeed(1)
#  SetSinglePrecision()

  # Ranges of variables 
  bounds = [ (5.15, 5.5) ]

  # Definition of the phase space
  phsp = RectangularPhaseSpace( bounds )

  outfile = "MassFit"        # Prefix for the output files 

  threads = 8                # Maximum number of CPU threads

  mb      = FitParameter("mb",     5.279, 5.25, 5.35, 0.001)
  stmb    = FitParameter("stmb",   5.279, 5.25, 5.35, 0.001)
  sigma   = FitParameter("sigma",   0.02, 0.001, 0.1, 0.0001)
  stsigma = FitParameter("stsigma", 0.02, 0.001, 1., 0.0001)
  alpha   = FitParameter("alpha",   1.2 ,   0., 5., 0.001)
  bkgfrac = FitParameter("bkgfrac", 0.2 ,   0., 20., 0.001)
  stfrac  = FitParameter("stfrac",  0.1 ,   0.1, 20., 0.001)

  def Gauss(x, m, sigma) : 
    d = (x-m)/sigma
    return Exp(-0.5*d**2)

  def Student(x, m, sigma) : 
    d = (x-m)/sigma
    return 1./(1.+d**2)

  def ExpBkg(x, alpha) : 
    return Exp(-alpha*x)

#  mb.fix()
#  stmb.fix()
#  alpha.fix()
#  sigma.fix()
#  stsigma.fix()

  # Define the fitting model (generic multilayer perceptron)
  def model(x) : 
    m = phsp.Coordinate(x, 0)
#    return signorm*CrystalBall(m, mb, sigma, alpha, n) + Exp( -beta*(m-Const(5.2)) )
    return Gauss(m, mb, sigma) + stfrac*Student(m, stmb, stsigma) + bkgfrac*ExpBkg(m-5.3, alpha)

  # Placeholders for data and normalisation tensors
  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  # TF graphs for data and normalisation
  data_model = model(data_ph)
  norm_model = model(norm_ph)

  # Define loss and optimizer
  nll = UnbinnedNLL( data_model, Integral( norm_model ) )

  init = tf.global_variables_initializer()

  session_conf = tf.ConfigProto( intra_op_parallelism_threads=threads, inter_op_parallelism_threads=threads)

  data_sample = LoadTransformArray("/home/poluekt/cernbox/pid/data/pidcalib_BJpsiEE_MD_TAGCUT.root", "DecayTree", variables = [ "B_M_DTF_Jpsi" ])
  data_sample = rec2array(data_sample)*0.001

  with tf.Session(config = session_conf) as sess :
    sess.run(init)

    norm_sample = sess.run( phsp.RectangularGridSample( (10000, ) ) )
    data_sample = sess.run( phsp.Filter(data_sample) )

    display = MultidimDisplay(data_sample, ["M"], phsp.Bounds(), pull = True, bins1d = 100 )

    result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = True )

    norm_pdf = sess.run(norm_model, feed_dict = { norm_ph : norm_sample } )
    display.draw(norm_sample, norm_pdf, outfile + ".pdf")

    print("Optimization Finished!")

if __name__ == "__main__" : 
  main()
