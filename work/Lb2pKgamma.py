# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf
import numpy as np
from itertools import product
from math import sqrt

# Timer for benchmarking
from timeit import default_timer as timer

import sys, os
# This should point to TensorFlowAnalysis package
sys.path.append("../")
from TensorFlowAnalysis import *

import rootpy.ROOT as ROOT

if __name__ == "__main__" : 

  cache = False       # Controls caching of helicity tensors (this speeds up calculations)
  norm_grid = 400     # Size of the 2D normalisation grid
  toy_sample = 10000  # Size of the toy MC sample to be fitted
  gradient = True     # Controls usage of analytic gradient in the minimisation

  # Masses of initial and final state particles
  mlb = 5.620   # Lambda_b0 mass in GeV
  mk  = 0.493   # K+ mass
  mp  = 0.938   # proton mass

  # Create phase space object for 3-body decays
  # Use only part of the phase space up to 2 GeV in the pK inv. mass (where most of the resonances are)
  phsp = DalitzPhaseSpace(mk, mp, 0., mlb, mabrange = (mk+mp, 2.) )

  # Blatt-Weisskopf radii
  db = Const(5.)
  dr = Const(1.5)

  # Switches (boolean flags) allow switching on-off of various amplitude components.
  # Used for calculation of fit fractions or producing component weights in fitted distributions for control plots.
  # Here we create 3 switches for 3 resonance components
  switches = Switches(3)

  # Lambda(1405), J^P=1/2^-
  mass_l1405 = Const(1.405)
  width_l1405 = Const(0.050)

  # Lambda(1600), J^P=1/2^+
  mass_l1600 = Const(1.600)
  width_l1600 = Const(0.150)

  # Lambda(1520), J^P=3/2^-
  mass_l1520 = Const(1.5195)
  width_l1520 = Const(0.0156)

  # LS couplings for Lambda(1405). Only up to L=1. Dictionary of the form: 
  #   (2L, 2S) : complex_coupling
  # (note that all angular momenta and spins are doubled!)
  l1405_ls_couplings = {
    (0, 1) : Complex(FitParameter("L1405_01_Re", 3., -20., 20., 0.01), FitParameter("L1405_01_Im", 0., -20., 20., 0.01)), 
    (2, 1) : Complex(FitParameter("L1405_21_Re", 0., -20., 20., 0.01), FitParameter("L1405_21_Im", 0., -20., 20., 0.01)), 
    (2, 3) : Complex(FitParameter("L1405_23_Re", 0., -20., 20., 0.01), FitParameter("L1405_23_Im", 0., -20., 20., 0.01)), 
  }

  l1600_ls_couplings = {
    (0, 1) : Complex(FitParameter("L1600_01_Re", 5., -20., 20., 0.01), FitParameter("L1600_01_Im", 0., -20., 20., 0.01)), 
    (2, 1) : Complex(FitParameter("L1600_21_Re", 0., -20., 20., 0.01), FitParameter("L1600_21_Im", 0., -20., 20., 0.01)), 
    (2, 3) : Complex(FitParameter("L1600_23_Re", 0., -20., 20., 0.01), FitParameter("L1600_23_Im", 0., -20., 20., 0.01)), 
  }

  l1520_ls_couplings = {
    (0, 1) : Complex(Const(1.), Const(0.)), 
    (2, 1) : Complex(FitParameter("L1520_21_Re", 0., -20., 20., 0.01), FitParameter("L1520_21_Im", 0., -20., 20., 0.01)), 
    (2, 3) : Complex(FitParameter("L1520_23_Re", 0., -20., 20., 0.01), FitParameter("L1520_23_Im", 0., -20., 20., 0.01)), 
  }

  # Fit model description as a function of tensor x which represents data/normalisation sample
  def model(x) : 

    # Get Dalitz plot variables 
    m2kp  = phsp.M2ab(x)
    m2pgamma = phsp.M2bc(x)

    # Calculate 4-momenta for the given Dalitz variables. 
    # Here we assume Lambda_b0 to be unpolarised, so it does not matter how the frame is rotated
    # For Dalitz plot, the 4-momenta are calculated in the rest frame of the decaying particle. 
    p4k, p4p, p4gamma = phsp.FinalStateMomenta(m2kp, m2pgamma)
    # Helicity angles for the 3-body decay: 
    #   theta_r, phi_r are the polar and azimuthal angles for pK resonance in Lambda_b0 rest frame
    #   theta_kp, phi_pk are the polar and azimuthal angles for K in pK resonance rest frame
    theta_r, phi_r, theta_kp, phi_kp = HelicityAngles3Body(p4k, p4p, p4gamma)

    # Initialise Dalitz plot density
    density = Const(0.)

    # Calculate line shape functions for the given pK mass
    # For each resonance we take lowest and second-to lowest orbital momenta for the Lambda_b0 -> Rgamma decay. 
    # On the other hand, the orbital momentum of R->pK decay is fixed by parity conservation. 
    # 
    # L(1405) J^P=1/2^-, so decay to p(1/2+) K(0-) is in S-wave (L=0)
    # Lb(1/2+) -> L(1405) gamma decay can be both L=0 and L=1 (no parity conservation in weak decay)
    l1405_bw_0 = SubThresholdBreitWignerLineShape(m2kp, mass_l1405, width_l1405, mk, mp, 0., mlb, dr, db, 0, 0)
    l1405_bw_1 = SubThresholdBreitWignerLineShape(m2kp, mass_l1405, width_l1405, mk, mp, 0., mlb, dr, db, 0, 1)

    # L(1600) J^P=1/2^+, decay to pK is in P-wave (L=1) due to parity conservation
    # Lb(1/2+) -> L(1600) gamma decay can be both L=0 and L=1
    l1600_bw_0 = BreitWignerLineShape(m2kp, mass_l1600, width_l1600, mk, mp, 0., mlb, dr, db, 1, 0)
    l1600_bw_1 = BreitWignerLineShape(m2kp, mass_l1600, width_l1600, mk, mp, 0., mlb, dr, db, 1, 1)

    # L(1520) J^P=3/2^-, decay to pK is in D-wave (L=2)
    # Lb(1/2+) -> L(1520) gamma decay can be L=0,1,2, only take L=0 and L=1 here
    l1520_bw_0 = BreitWignerLineShape(m2kp, mass_l1520, width_l1520, mk, mp, 0., mlb, dr, db, 2, 0)
    l1520_bw_1 = BreitWignerLineShape(m2kp, mass_l1520, width_l1520, mk, mp, 0., mlb, dr, db, 2, 1)

    # Dictionaries of LS couplings multiplied by the corresponding BW shapes (which depend on L)
    l1405_ls_ampl = {
      (0, 1) : l1405_ls_couplings[ (0, 1) ]*l1405_bw_0, 
      (2, 1) : l1405_ls_couplings[ (2, 1) ]*l1405_bw_1, 
      (2, 3) : l1405_ls_couplings[ (2, 3) ]*l1405_bw_1, 
    }

    l1600_ls_ampl = {
      (0, 1) : l1600_ls_couplings[ (0, 1) ]*l1600_bw_0, 
      (2, 1) : l1600_ls_couplings[ (2, 1) ]*l1600_bw_1, 
      (2, 3) : l1600_ls_couplings[ (2, 3) ]*l1600_bw_1, 
    }

    l1520_ls_ampl = {
      (0, 1) : l1520_ls_couplings[ (0, 1) ]*l1520_bw_0, 
      (2, 1) : l1520_ls_couplings[ (2, 1) ]*l1520_bw_1, 
      (2, 3) : l1520_ls_couplings[ (2, 3) ]*l1520_bw_1, 
    }

    # Nested loop over all polarisations of the Lambda_b (mu_lb), proton (lambda_p) and photon (lambda_gamma)
    # The amplitudes will be summed up incoherently over them 
    for (mu_lb, lambda_p, lambda_gamma) in product( [-1, 1], [-1, 1], [-2, 2] ) : 

      # Initialise complex amplitude for a given polarisation
      ampl = Complex(Const(0.), Const(0.))

      # Convert LS couplings to helicity couplings
      # Each Lambda* resonance can have helicities +1/2 and -1/2 (denoted "p" and "m")
      l1405_hel_p_coupling = HelicityCouplingsFromLS(1, 1, 2,  1, lambda_gamma, l1405_ls_ampl)
      l1405_hel_m_coupling = HelicityCouplingsFromLS(1, 1, 2, -1, lambda_gamma, l1405_ls_ampl)

      l1600_hel_p_coupling = HelicityCouplingsFromLS(1, 1, 2,  1, lambda_gamma, l1600_ls_ampl)
      l1600_hel_m_coupling = HelicityCouplingsFromLS(1, 1, 2, -1, lambda_gamma, l1600_ls_ampl)

      l1520_hel_p_coupling = HelicityCouplingsFromLS(1, 1, 2,  1, lambda_gamma, l1520_ls_ampl)
      l1520_hel_m_coupling = HelicityCouplingsFromLS(1, 1, 2, -1, lambda_gamma, l1520_ls_ampl)

      # Coherently sum up all the resonance amplitudes with two polarisations each
      ampl += Complex(switches[0], Const(0.))*l1405_hel_p_coupling*\
                  HelicityAmplitude3Body(theta_r, phi_r, theta_kp, phi_kp, 1, 1, mu_lb,  1, 0, lambda_p,  2, cache = cache)
      # Note (-1) or (+1) in front of lambda_r = -1/2 amplitude, which depends on the spin/parity
      # of the intermediate resonance
      ampl += (-1)*Complex(switches[0], Const(0.))*l1405_hel_m_coupling*\
                  HelicityAmplitude3Body(theta_r, phi_r, theta_kp, phi_kp, 1, 1, mu_lb, -1, 0, lambda_p, -2, cache = cache)

      ampl += Complex(switches[1], Const(0.))*l1600_hel_p_coupling*\
                  HelicityAmplitude3Body(theta_r, phi_r, theta_kp, phi_kp, 1, 1, mu_lb,  1, 0, lambda_p,  2, cache = cache)
      ampl += Complex(switches[1], Const(0.))*l1600_hel_m_coupling*\
                  HelicityAmplitude3Body(theta_r, phi_r, theta_kp, phi_kp, 1, 1, mu_lb, -1, 0, lambda_p, -2, cache = cache)

      ampl += Complex(switches[2], Const(0.))*l1520_hel_p_coupling*\
                  HelicityAmplitude3Body(theta_r, phi_r, theta_kp, phi_kp, 1, 3, mu_lb,  1, 0, lambda_p,  2, cache = cache)
      ampl += Complex(switches[2], Const(0.))*l1520_hel_m_coupling*\
                  HelicityAmplitude3Body(theta_r, phi_r, theta_kp, phi_kp, 1, 3, mu_lb, -1, 0, lambda_p, -2, cache = cache)

      density += Density(ampl)

    return density

  # Data and normalisation sample placeholders
  data_ph = phsp.data_placeholder
  norm_ph = phsp.norm_placeholder

  # TF graphs for data and normalisation that will enter unbinned likelihood
  data_pdf = model(data_ph)
  norm_pdf = model(norm_ph)

  # Set inital random seed and open TF session
  SetSeed(1)
  sess = tf.Session()

  # Initialisation of TF fit variables
  init = tf.global_variables_initializer()
  sess.run(init)

  # Produce normalisation sample (rectangular 2D grid of points over the kinematic phase space)
  norm_sample = sess.run( phsp.RectangularGridSample(norm_grid, norm_grid) )
  print("Normalisation sample size = ", len(norm_sample))

  # Calculate maximum of the PDF for accept-reject toy MC sampling
  majorant = EstimateMaximum(sess, data_pdf, data_ph, norm_sample )*1.5
  print("Maximum = ", majorant)

  # Create toy MC data sample
  data_sample = RunToyMC( sess, data_pdf, data_ph, phsp, toy_sample, majorant, chunk = 1000000)

  # Store it in ROOT NTuple
  f = ROOT.TFile.Open("Lb2pKgamma_gentoy.root", "RECREATE")
  FillNTuple("toy", data_sample, ["m2kp", "m2pgamma" ] )
  f.Close()

  # Calculate fit fractions for individual components and store then in a text file
  ff = CalculateFitFractions(sess, data_pdf, data_ph, switches, norm_sample = norm_sample )
  WriteFitFractions(ff, ["L1405", "L1600", "L1520"], "Lb2pKgamma_fitfractions_init.txt")

  # Define unbinned negative log likelihood
  nll = UnbinnedNLL( data_pdf, Integral( norm_pdf) )
  start = timer()
  # Run Munuit minimisation of the NLL
  result = RunMinuit(sess, nll, { data_ph : data_sample, norm_ph : norm_sample }, useGradient = gradient )
  end = timer()

  # Store fit result in a text file
  print("Fit results:")
  print(result)
  WriteFitResults(result, "Lb2pKgamma_result.txt")

  # Calculate fit fractions for the fitted amplitude
  # This operation needs the switches we've defined before
  ff = CalculateFitFractions(sess, data_pdf, data_ph, switches, norm_sample = norm_sample)
  # Write fit fractions to the text file
  WriteFitFractions(ff, ["L1405", "L1600", "L1520"], "Lb2pKgamma_fitfractions.txt")

  # Create toy MC sample corresponding to the fitted distribution with weights for individual components
  # (the latter is controlled by providing "switches = ..." argument) and store it in a ROOT NTuple
  fit_sample = RunToyMC(sess, data_pdf, data_ph, phsp, 100000, majorant, chunk = 1000000, switches = switches)
  f = ROOT.TFile.Open("Lb2pKgamma_fittoy.root", "RECREATE")
  FillNTuple("toy", fit_sample, ["m2pk", "m2pgamma" ] + [ "w%d" % (n+1) for n in range(len(switches)) ])
  f.Close()

  # Plot generated distribution and fit result
  h1 = ROOT.Hist2D(100, phsp.minab, phsp.maxab, 100, phsp.minbc, phsp.maxbc ) 
  h2 = ROOT.Hist2D(100, phsp.minab, phsp.maxab, 100, phsp.minbc, phsp.maxbc ) 
  h3 = ROOT.Hist(100, sqrt(phsp.minab), sqrt(phsp.maxab) ) 
  h4 = ROOT.Hist(100, sqrt(phsp.minab), sqrt(phsp.maxab) ) 
  h5 = ROOT.Hist(100, sqrt(phsp.minbc), sqrt(phsp.maxbc) ) 
  h6 = ROOT.Hist(100, sqrt(phsp.minbc), sqrt(phsp.maxbc) ) 

  h1.fill_array( data_sample )
  h3.fill_array( np.sqrt(data_sample[:,0]) )
  h5.fill_array( np.sqrt(data_sample[:,1]) )

  h2.fill_array( fit_sample[:,0:2] )
  h4.fill_array( np.sqrt(fit_sample[:,0]) )
  h6.fill_array( np.sqrt(fit_sample[:,1]) )

  # Calculate fit components in the 1D projections from the component weights
  comp_h1 = []
  comp_h2 = []
  for n in range(len(switches)) : 
    comp_h1 += [ ROOT.Hist(100, sqrt(phsp.minab), sqrt(phsp.maxab) ) ]
    comp_h2 += [ ROOT.Hist(100, sqrt(phsp.minbc), sqrt(phsp.maxbc) ) ]
    # Fill projection with the weight of each component
    comp_h1[-1].fill_array( np.sqrt(fit_sample[:,0]), fit_sample[:,2+n] )
    comp_h2[-1].fill_array( np.sqrt(fit_sample[:,1]), fit_sample[:,2+n] )

  # Open ROOT canvas and draw stuff
  c = ROOT.TCanvas("c","", 600, 600)
  c.Divide(2, 2)
  h4.SetLineColor(2)
  h6.SetLineColor(2)
  c.cd(1); h1.Draw("zcol")
  c.cd(2); h2.Draw("zcol")

  c.cd(3)
  h3.Draw("e")
  scale = h3.Integral()/h4.Integral()
  h4.Scale(scale)
  h4.Draw("hist same")
  for n,h in enumerate(comp_h1) : 
    h.Scale(scale)
    h.SetColor(3+n)
    h.Draw("hist same")

  c.cd(4)
  h5.Draw("e")
  scale = h5.Integral()/h6.Integral()
  h6.Scale(scale)
  h6.Draw("hist same")
  for n,h in enumerate(comp_h2) : 
    h.Scale(scale)
    h.SetColor(3+n)
    h.Draw("hist same")

  c.Update()
  c.Print("Lb2pKgamma.pdf")

  print("Fitting time: %f sec" % (end - start)) 
