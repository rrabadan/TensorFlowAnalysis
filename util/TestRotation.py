import tensorflow as tf

import sys
sys.path.append("../lib/")

from ROOT import TH1F, TH2F, TCanvas
from Interface import *
from Kinematics import *
from Dynamics import *
from Optimisation import *

sess = tf.Session()
init = tf.initialize_all_variables()
sess.run(init)

SetSeed(0)

mlc = 2.286
mp  = 0.938
mk  = 0.498
mpi = 0.140

phsp = Baryonic3BodyPhaseSpace(mp, mk, mpi, mlc)

sample = sess.run(phsp.UniformSample(2))
print sample

m2pk =  phsp.M2ab(sample)
m2kpi = phsp.M2bc(sample)

p4p, p4k, p4pi = phsp.FinalStateMomenta(m2pk, m2kpi, 0., 0., 0.)

p4lst = p4p + p4k
p4kst = p4k + p4pi
p4dst = p4p + p4pi

ps = [ p4p, p4k, p4pi ]

print sess.run(ps)

# 1st decay chain via L* (pK resonances)
theta1a, phi1a = HelicityAngles( p4lst ) # L* helicity
ps1a = RotationAndBoost(ps, p4lst)

theta1b, phi1b = HelicityAngles( ps1a[0] ) # Proton helicity in N* frame
ps1b = RotationAndBoost(ps1a, ps1a[0])

# 2nd decay chain via Delta* (ppi resonances)
theta2a, phi2a = HelicityAngles( p4dst ) # D* helicity
ps2a = RotationAndBoost(ps, p4dst)

theta2b, phi2b = HelicityAngles( ps2a[0] ) # Proton helicity in D* frame
ps2b = RotationAndBoost(ps2a, ps2a[0])


prot = RotationAndBoost([ p4lst, p4dst ], p4p)
th1, ph1 = SphericalAngles( prot[0] )
th2, ph2 = SphericalAngles( prot[1] )

prot2 = RotationAndBoost([ p4k, p4pi ], p4p)
th3, ph3 = SphericalAngles( prot2[0] )
th4, ph4 = SphericalAngles( prot2[1] )

thetaa, phia, thetab, phib = HelicityAngles3Body(p4p, p4k, p4pi)
rot = SpinRotationAngle(p4k, p4p, p4pi, 0)

print "p4lc: ",   sess.run(p4lst)
print "p4lb: ",   sess.run(p4kst)

print "thetaa: ", sess.run(thetaa)
print "phia: ",   sess.run(phia)
print "thetab: ", sess.run(thetab)
print "phib: ",   sess.run(phib)

print "theta1a: ", sess.run(theta1a)
print "phi1a: ",   sess.run(phi1a)
print "theta1b: ", sess.run(theta1b)
print "phi1b: ",   sess.run(phi1b)
print "transformed1a p4p: ", sess.run(ps1a[0])
print "transformed1a p4k: ", sess.run(ps1a[1])
print "transformed1a p4pi: ", sess.run(ps1a[2])
print "transformed1b p4p: ", sess.run(ps1b[0])
print "transformed1b p4k: ", sess.run(ps1b[1])
print "transformed1b p4pi: ", sess.run(ps1b[2])

print "theta2a: ", sess.run(theta2a)
print "phi2a: ",   sess.run(phi2a)
print "theta2b: ", sess.run(theta2b)
print "phi2b: ",   sess.run(phi2b)
print "transformed2a p4p: ", sess.run(ps2a[0])
print "transformed2a p4k: ", sess.run(ps2a[1])
print "transformed2a p4pi: ", sess.run(ps2a[2])
print "transformed2b p4p: ", sess.run(ps2b[0])
print "transformed2b p4k: ", sess.run(ps2b[1])
print "transformed2b p4pi: ", sess.run(ps2b[2])

print "rot: ",  sess.run(rot)

print "theta1: ",  sess.run(th1)
print "theta2: ",  sess.run(th2)
print "phi1: ",  sess.run(ph1)
print "phi2: ",  sess.run(ph2)
print "theta3: ",  sess.run(th3)
print "theta4: ",  sess.run(th4)
print "phi3: ",  sess.run(ph3)
print "phi4: ",  sess.run(ph4)
